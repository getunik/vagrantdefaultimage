
exec { "apt-update":
  command => "/usr/bin/apt-get update",
  subscribe   => File["/etc/apt/sources.list"],
  require     => File["/etc/apt/sources.list"],
}

Exec["apt-update"] -> Package <| |>

#install apache 
class { "apache":
  default_vhost  => false,
  apache_version => 2.4,
  mpm_module => false,
  user => vagrant,
  group => vagrant,
}

class { "mysql::server":
  restart => true,
}

mysql_user { 'getunik@%':
  ensure                   => 'present',
  max_connections_per_hour => '0',
  max_queries_per_hour     => '0',
  max_updates_per_hour     => '0',
  max_user_connections     => '0',
  password_hash            => '*A54EADCD1B1CE427FDB176042E87C63107E8D4E1',
  require => Class["mysql::server"]
}

mysql_grant { 'getunik@%/*.*':
  ensure     => 'present',
  options    => ['GRANT'],
  privileges => ['ALL'],
  table      => '*.*',
  user       => 'getunik@%',
  require => Class["mysql::server"]
}

#add ppa for php5
apt::ppa { 'ppa:ondrej/php5':
  before  => Class['php'],
}

apt_key { 'ondrej':
  ensure => 'present',
  id     => 'E5267A6C',
}
 
include php
include phpmyadmin
include apt

#install apache mods
include apache::mod::prefork
include apache::mod::rewrite
include apache::mod::headers
include apache::mod::ssl
include apache::mod::php


#create directory for dev.localhost/Symfony/web docroot
file { [ "/var/www/dev.localhost", "/var/www/dev.localhost/Symfony"]:
  ensure => "directory",
  owner => $::apache::params::user,
  group => $::apache::params::group,
  mode => "755",
}


#create vhost for dev.localhost/Symfony/web docroot Port 80
apache::vhost{"dev.localhost":
  port  => "80",
  docroot => "/var/www/dev.localhost/Symfony/web",
  block => ["scm"],
  override => ["All"],
  require => File["/var/www/dev.localhost/Symfony/web"],
}

#create vhost for dev.localhost/Symfony/web docroot Port 443
apache::vhost{"dev.localhost_ssl":
  port  => "443",
  ssl => true,
  docroot => "/var/www/dev.localhost/Symfony/web",
  block => ["scm"],
  override => ["All"],
  require => File["/var/www/dev.localhost/Symfony/web"],
}

#symlink var/www
file { "/var/www/dev.localhost/Symfony/web":
   ensure => "link",
   target => "/vagrant/web",
   force => true,
   mode => "755",
    
}





