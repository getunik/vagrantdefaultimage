# Class: phpmyadmin
#
#
class phpmyadmin {
	package { "phpmyadmin":
		ensure => latest,
		require => Class["php"],
		notify => Class["apache::service"],
	}

	file { "/etc/apache2/conf.d/phpmyadmin.conf":
      	 ensure => link,
   		 target => "/etc/phpmyadmin/apache.conf",
    	 require => Package['phpmyadmin'],
    	 notify => Class["apache::service"],
    }

    file { "/etc/apache2/mods-enabled/authn_core.load":
      	 ensure => link,
   		 target => "/etc/apache2/mods-available/authn_core.load",
    	 require => Package['phpmyadmin'],
    	 notify => Class["apache::service"],
    }
}