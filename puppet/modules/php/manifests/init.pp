class php{


	package { "php5":
		ensure => present,
		notify  => Class["apache::service"],
	}



	package { "php5-curl":
		ensure => present,
		notify  => Class["apache::service"],
		require => Package["php5"],
	}

	package { "php5-mcrypt":
		ensure => present,
		notify  => Class["Apache::Service"],
		require => Package["php5"],
	}

	package { "php5-mysql":
		ensure => present,
		notify  => Class["Apache::Service"],
		require => Package["php5"],
	}

	package { "php5-sqlite":
		ensure => present,
		notify  => Class["Apache::Service"],
		require => Package["php5"],
	}


	file { "php.ini":
		ensure => present,
		path => "/etc/php5/cli/php.ini",
		content => template("php/php.ini.erb"),
		notify  => Class["Apache::Service"],
		require => Package ["php5"],
	}

	file { "apache/php.ini":
		ensure => present,
		path => "/etc/php5/apache2/php.ini",
		content => template("php/php.ini.erb"),
		notify  => Class["Apache::Service"],
		require => Package ["php5"],
	}

	file { "/etc/php5/apache2/conf.d/20-mcrypt.ini":
      	 ensure => link,
   		 target => "/etc/php5/mods-available/mcrypt.ini",
    	 require => Package['php5-mcrypt'],
    	 notify => Class["apache::service"],
    }

    file { "/etc/php5/cli/conf.d/20-mcrypt.ini":
      	 ensure => link,
   		 target => "/etc/php5/mods-available/mcrypt.ini",
    	 require => Package['php5-mcrypt'],
    	 notify => Class["apache::service"],
    }
}